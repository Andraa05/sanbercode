<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
  public function index()
  {
    return view('/Login.login');
  }

  public function login(Request $request)
  {
    $getuser = User::where('email', '=', $request->email)->first();
    if ($getuser != null) {
      if (Hash::check($request->password, $getuser->password) == true) {
        Auth::guard('auth')->login($getuser);
        return redirect()->route('home');
      }
    };
  }
  public function logout(Request $request)
  {
    Auth::guard('auth')->logout();

    $request->session()->invalidate();

    $request->session()->regenerateToken();

    return redirect()->route('login.index');
  }
}
