@extends('layouts.dashboard')

@section('title')
    Halaman Tambah kategori
@endsection

@section('content')
<div class="row mb-3">
  <div class="col-lg-12">
    <button type="button" class="btn btn-sm btn-primary" id="btn-w-modal">
      New category
    </button>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
              <tr>
                  <th>#</th>
                  <th>Category Name</th>
                  <th>Actions</th>
              </tr>
          </thead>
          <tbody>
            @foreach ($daftarkategori as $item)
                <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{$item->name}}</td>
                  <td>
                    <form action="/kategori/{{{$item->id}}}" method="POST" class="d-flex flex-lg-row flex-column justify-content-center align-items-center g-4 gap-3">
                        @csrf
                        @method('delete')
                        <a href="/kategori/{{$item->id}}" class="btn btn-info btn-sm mx-3 lg-my-0 my-3">Detail</a>
                        <a href="/kategori/{{$item->id}}/edit" class="btn btn-warning btn-sm mx-3 lg-my-0 my-3">Edit</a>
    
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
                </tr>
            @endforeach
          </tbody>
      </table>
    </div>
  </div>
</div>

{{-- MODAL --}}
<div class="modal" tabindex="-1" id="modal-tambah-kategori">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('kategori.store')}}" method="post">
        @csrf
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-12">
            <input type="text" class="form-control" placeholder="Nama kategori..." name="nama_kategori">
          </div>
        </div>
      </div>
   
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
</form>
@endsection

@push('scripts')
    <script>
      $('#dataTable').DataTable({
        
      });

      $('#btn-w-modal').click(function (e) {
        e.preventDefault();

        $('#modal-tambah-kategori').modal('show');
      })
    </script>
@endpush