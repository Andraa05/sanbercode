@extends('layouts.dashboard')

@section('title')
    {{$kategori->name}}
@endsection

@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
              <tr>
                  <th>#</th>
                  <th>Content</th>
                  <th>Created by</th>
                  <th>categories</th>
                  <th>Actions</th>
              </tr>
          </thead>
          <tbody>
            @foreach ($listPertanyaan as $item)
                <tr>
                  <td>{{ $loop->iteration }}</td>
                  <td>{{ $item->tulisan }}</td>
                  <td>{{ $item->user->name }}</td>
                  <td>{{ $item->kategori->name }}</td>
                  <td>
                    <a href="" class="btn btn-sm btn-success">Detail</a>
                  </td>
                </tr>
            @endforeach
          </tbody>
      </table>
    </div>
  </div>
</div>
@endsection

@push('scripts')
    <script>
      $('#dataTable').DataTable();
    </script>
@endpush