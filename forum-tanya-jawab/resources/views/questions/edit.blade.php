@extends('layouts.dashboard')

@section('title')
    Halaman Edit Kategori
@endsection

@section('sub-title')
    Kategori
@endsection

@section('content')
<form action="/kategori/{{$categories->id}}" method="post">
    @csrf
    @method('put')
    <div class="form-group mb-3">
      <label>Category Name :</label>
      <input type="text" name="name" value="{{$categories->name}}" class="form-control">
    </div>

    @error('name')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group mb-3">
        <label>Description :</label>
        <textarea class="form-control" name="description" rows="3">{{$categories->description}}</textarea>
    </div>

    @error('description')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <button type="submit" class="btn btn-primary w-100 mb-3">Submit</button>
    <a href="/kategori" class="btn btn-danger w-100">Cancel</a>
  </form>
@endsection