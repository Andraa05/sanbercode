@extends('layouts.dashboard')

@section('title')
    Halaman Pertanyaan
@endsection

@section('content')
<a href="/question/create" class="btn btn-primary btn-sm my-3">Buat Pertanyaan</a>

<div class="row row-cols-2 row-cols-md-4 g-4">
    <div class="col">
        <div class="card">
          <img src="{{asset('/img/' . $item->gambar)}}" height="200" class="card-img-top" alt="Gambar Pertanyaan">
          <div class="card-body">
            <h5 class="card-title">{{$item->tulisan}}</h5>
            <span class="badge text-bg-secondary mb-3">{{$item->kategori}}</span>
          </div>
        </div>
      </div>
    </div>
    <a href="/question" class="btn btn-primary btn-sm">Kembali</a>
</div>
@endsection