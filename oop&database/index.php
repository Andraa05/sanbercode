<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal ("Shaun");
echo "Name : " . $sheep->name . "<br>";
echo "Legs : " . $sheep->legs . "<br>";
echo "Cold Blooded : " . $sheep->cold_blooded . "<br>";
echo "<br>";


$kodok = new Frog ("buduk");
echo "Name : " . $kodok->name . "<br>";
echo "Legs : " . $kodok->legs . "<br>";
echo "Cold Blooded : " . $kodok->cold_blooded . "<br>";
echo "Jump : " . $kodok->jump . "<br>" ;
echo "<br>";


$Sungokong = new Ape ("Kera Sakti");
echo "Name : " . $Sungokong->name . "<br>";
echo "Legs : " . $Sungokong->legs . "<br>";
echo "Cold Blooded : " . $Sungokong->cold_blooded . "<br>";
echo "Yell : " . $Sungokong->yell . "<br>" ;

?>